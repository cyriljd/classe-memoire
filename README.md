# Classe memoire

Classe mémoire pour LaTeX.

<img src="memoire.png"  width="240" >

Cette classe est adaptée à un mémoire de Master ASA de l'université de Bourgogne.

*  Télécharger le fichier [memoire.cls](https://framagit.org/cyriljd/classe-memoire/raw/master/memoire/memoire.cls?inline=false)
*  Placer le fichier memoire.cls dans le dossier de votre projet LaTeX
*  La configuration est expliquée dans le [wiki](https://framagit.org/cyriljd/classe-memoire/-/wikis/home).

Licence [WTFPL](http://www.wtfpl.net/)
