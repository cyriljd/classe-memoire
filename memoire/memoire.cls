%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%% MÉMOIRE V 1.0 %%%%%%%%%%
%
% Classe mémoire pour latex
% Adapté au master ASA d'archéologie de l'université de Bourgogne-Franche-Comté
% Basée sur la classe Bredele v 3.0
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{memoire}[14/09/2010 v2.0]

\LoadClass[oneside,12pt,a4paper]{book}
\RequirePackage[utf8]{luainputenc}
\RequirePackage[french]{babel}
\RequirePackage{ifpdf, color}

\RequirePackage{luatextra}
%\RequirePackage[T1]{fontenc} % permet de spécifier à LaTeX l'utilisation du codage de caractères T1, nouvelle norme LaTeX non utilisée par défaut pour des raisons de compatibilité avec les anciens documents LaTeX.
\RequirePackage{lmodern}
\RequirePackage{tikz}
\RequirePackage{fix-cm}
\RequirePackage{graphicx}
\RequirePackage{pdfpages}
\RequirePackage{sectsty}
\RequirePackage{shorttoc}
\RequirePackage{fancyhdr}
\RequirePackage{pifont}
\RequirePackage{lscape}
\RequirePackage[top=2 cm, bottom=2 cm, left=2.5 cm, right=2.5 cm]{geometry}
\RequirePackage{subscript} % pour mettre du texte en indice
\RequirePackage{setspace} % permet d'utiliser les commandes \spacing, doublespace (double interligne), singlespace (simple interligne) et onehalfspace (un interligne et demi)
\RequirePackage{hyperref} %Utiliser pour une bibliographie interactive + sommaire et TOC interactifs (pour une sortie PDF, par exemple).
\RequirePackage{framed}
\RequirePackage{color}
\RequirePackage{xr}
\RequirePackage{ifthen}
\RequirePackage{csquotes}

\hypersetup{colorlinks=true, citecolor=black, filecolor=black, linkcolor=black, urlcolor=black}
\onehalfspacing %Interligne 1,5
\frenchspacing
\FrenchFootnotes
\setlength{\parskip}{0.5cm plus4mm minus3mm}

% A ajouter dans hypersetup pour plus de visibilite dans les proprietes du document
%pdftitle={titre du document}, pdfauthor={auteur}, pdfcreator={PdfLaTeX}, pdfkeywords={mots-clé}, pdfsubject={sujet du document}

\addto\captionsfrench{
	\renewcommand{\contentsname}%
	{Sommaire}%
}

\addto\captionsfrench{\def\tablename{Tableau}}

\RequirePackage{rotating}
%\usepackage{float}
%pour régler la hauteur des lignes des tableaux
\renewcommand\arraystretch{1.5}

\renewcommand{\thesection}{\arabic{section}.}
\renewcommand{\thesubsection}{\arabic{section}.\arabic{subsection}.}
\renewcommand{\thesubsubsection}{\arabic{section}.\arabic{subsection}.\arabic{subsubsection}.}

\addto\captionsfrench{%
  \renewcommand{\listfigurename}{Liste des figures}
}

\setcounter{tocdepth}{3}
\setcounter{secnumdepth}{3}

%%%%%%%%%%%%%% En-tete de chapitre %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Les lignes suivantes définissent l'esthetique des débuts de chapitre.
% Cette sequence a ete recuperee sur Internet, createur inconnu...
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\makeatletter
\def\thickhrulefill{\leavevmode \leaders \hrule height 1ex \hfill \kern \z@}
\def\@makechapterhead#1{%
  \vspace*{-30\p@}%
  {\parindent \z@ \raggedleft \reset@font
            \scshape \@chapapp{} \thechapter
        \par\nobreak
        \interlinepenalty\@M
    \Huge \bfseries #1\par\nobreak
    %\vspace*{1\p@}%
    \hrulefill
    \par\nobreak
    \vskip 50\p@
  }}
\def\@makeschapterhead#1{%
 \vspace*{-50\p@}%
  {\parindent \z@ \raggedleft \reset@font
            \scshape \vphantom{\@chapapp{} \thechapter}
        \par\nobreak
        \interlinepenalty\@M
    \Huge \bfseries #1 \par\nobreak
    %\vspace*{1\p@}%
    \hrulefill
    \par\nobreak
    \vskip 30\p@
  }}

%%%%%%%%%%%%%%%%%%%% Fin en-tête de chapitre %%%%%%%%%%%%%%%%%%%

%%%%%%%%%%% Tete de parties %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\makeatletter
\newcommand*{\jolipart}[1]{%
	\begin{center}
		\begin{Huge}\color{black}
			#1
		\end{Huge}
		\end{center}
		\vspace{1cm}
	\begin{center}
%\begin{Huge}
		%\ding{167}
	%\end{Huge}
	\hrulefill
	\end{center}
	}

\parttitlefont{\jolipart}

\partnumberfont{\sc\normalsize}

\definecolor{grisfonce}{RGB}{49,49,49}
\definecolor{grisclair}{RGB}{111,111,111}
\definecolor{blanc}{RGB}{255,255,255}

%%%%%%%%%%%%%%%%%% REGLAGE DE LA BIBLIOGRAPHIE %%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%% ATTENTION, on utilise Biblatex %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Pour le style biblio
% \RequirePackage[]{bibspf}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
\let\orig@quote\quote
\let\endorig@quote\endquote
\renewenvironment*{quote}
	{\begin{orig@quote}\begin{singlespace}}
	{\end{singlespace}\end{orig@quote}}

\let\old@quotation\quotation
\let\endold@quotation\endquotation
\renewenvironment*{quotation}
	{\begin{old@quotation}\begin{singlespace}}
        {\end{singlespace}\end{old@quotation}}

\let\old@verse\verse
\let\endold@verse\endverse
\renewenvironment*{verse}
	{\begin{old@verse}\begin{singlespace}}
        {\end{singlespace}\end{old@verse}}

%%%%%%%%%%%%%%%% PAGE DE GARDE %%%%%%%%%%%%%%%%

%logo universite A

\newcommand*{\logouniversitea}[1]{\gdef\@logouniversitea{#1}}
\logouniversitea{}

\newcommand*{\scalelogouniversitea}[1]{\gdef\@scalelogouniversitea{#1}}
\scalelogouniversitea{}

%logo universite B

\newcommand*{\logouniversiteb}[1]{\gdef\@logouniversiteb{#1}}
\logouniversiteb{}

\newcommand*{\scalelogouniversiteb}[1]{\gdef\@scalelogouniversiteb{#1}}
\scalelogouniversiteb{}

% Logo Labo
\newcommand*{\logolabo}[1]{\gdef\@logolabo{#1}}
\logolabo{}

\newcommand*{\scalelogolabo}[1]{\gdef\@scalelogolabo{#1}}
\scalelogolabo{}

% Logo Master
\newcommand*{\logomaster}[1]{\gdef\@logomaster{#1}}
\logomaster{}

\newcommand*{\scalelogomaster}[1]{\gdef\@scalelogomaster{#1}}
\scalelogomaster{}

% Barre Logos
\newcommand*{\barrelogos}[1]{\gdef\@barrelogos{#1}}
\barrelogos{}

\newcommand*{\scalebarrelogos}[1]{\gdef\@scalebarrelogos{#1}}
\scalebarrelogos{}

%%%% Eventuel sous-titre
\newcommand{\lesoustitre}[1]{\gdef\@lesoustitre{#1}}
\lesoustitre{}

%%%% Nom du directeur
\newcommand{\directeur}[1]{\gdef\@directeur{#1}}
\directeur{}

%%%% Nom du tuteur
\newcommand{\tuteur}[1]{\gdef\@tuteur{#1}}
\tuteur{}

%%%% Université
\newcommand{\universite}[1]{\gdef\@universite{#1}}
\universite{}

%%%% UFR
\newcommand{\ufr}[1]{\gdef\@ufr{#1}}
\ufr{}

%%%% Master
\newcommand{\master}[1]{\gdef\@master{#1}}
\master{}

%%%% Spécialitée
\newcommand{\specialitee}[1]{\gdef\@specialitee{#1}}
\specialitee{}

%%%% Co-habilitation
\newcommand{\cohab}[1]{\gdef\@cohab{#1}}
\cohab{}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%mise en page de la page de garde
\makeatletter
\def\maketitle{%

	% première page
	\thispagestyle{empty}
	\clearpage
\vspace{1cm}
	\begin{tikzpicture}[remember picture,overlay,line width=0mm]
  \draw [draw=white,fill=white]
    (current page.north west) rectangle (\paperwidth,1);
  \node[xshift=7.3cm,y=2cm,text=white,font=\bf\Large] {
	\includegraphics[scale=\@scalebarrelogos]{images/logos/\@barrelogos}
  };
   \node at (current page.center)
{\parbox{15cm}{
\bigskip
\vspace{2cm}
	\begin{center}
		\@universite \\ \@ufr
	\end{center}

\begin{center}
\@master \\
\@specialitee \\
\@cohab
\end{center}

\vspace{2cm}
\noindent\rule{15cm}{0.5mm}
	\begin{center}
		{\setlength{\baselineskip}{1.4\baselineskip}
		\begin{Large}
			\textbf{\@title}
		\end{Large}
		\par}

		\large{\textit{\@lesoustitre}}
	\end{center}
\noindent\rule{15cm}{0.5mm}
	\begin{center}
		Volume de texte \\
		Master 2 \\
\vspace{2cm}
		Mémoire présenté par \\
		\textsc{\@author} \\
\vspace{0.5cm}

		Sous la direction de \textsc{\@directeur} \\% Tuteur: \textsc{\@tuteur} \\
\vspace{3cm}
		Année universitaire : \@date
	\end{center}
}
};
\end{tikzpicture}

% deuxième page
\thispagestyle{empty}
\clearpage
\vspace{1cm}
\begin{tikzpicture}[remember picture,overlay,line width=0mm]
\draw [draw=white,fill=white]
	(current page.north west) rectangle (\paperwidth,1);
 \node at (current page.center)
{\parbox{15cm}{
\bigskip
\vspace{2cm}
\begin{center}
	\@universite \\ \@ufr
\end{center}

\begin{center}
\@master \\
\@specialitee \\
\@cohab
\end{center}

\vspace{2cm}
\noindent\rule{15cm}{0.5mm}
\begin{center}
	{\setlength{\baselineskip}{1.4\baselineskip}
	\begin{Large}
		\textbf{\@title}
	\end{Large}
	\par}

	\large{\textit{\@lesoustitre}}
\end{center}
\noindent\rule{15cm}{0.5mm}
\begin{center}
	Volume de texte \\
	Master 2 \\
\vspace{2cm}
	Mémoire présenté par \\
	\textsc{\@author} \\
\vspace{0.5cm}

	Sous la direction de \textsc{\@directeur} \\% Tuteur: \textsc{\@tuteur} \\
\vspace{3cm}
	Année universitaire : \@date
\end{center}
}
};
\end{tikzpicture}

}

\makeatother



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%% En-tetes %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%% Pour suppirmer les entetes et pied de page gênants par exemple juste avant un chapitre sur une page de droite
\newcommand{\clearemptydoublepage}{%
   \newpage{\pagestyle{empty}\cleardoublepage}}
%%%% .... et utiliser la commande \clearemptydoublepage juste avant le \chapter


    \fancyhf{}                       % on annule le fancy automatique


%%%%%%%%%% Gerer les en tetes dans les frontmatter mainmatter et backmatter

\RequirePackage{etoolbox}

\appto\frontmatter{\pagestyle{fancy}
\renewcommand{\sectionmark}[1]{}
\renewcommand{\chaptermark}[1]{\markboth{\textit{#1}}{}}
\fancyhead[LE,RO]{\small\thepage}
    \fancyhead[RO]{\small\leftmark}
    \fancyhead[LE]{\small\leftmark}
    \fancyfoot[C]{\thepage}
}

\appto\mainmatter{\pagestyle{fancy}
\renewcommand{\sectionmark}[1]{\markright{\textit{\thesection.\ #1}}}
\renewcommand{\chaptermark}[1]{\markboth{\chaptername~\thechapter~--\ \textit{#1}}{}}
\fancyhead[LE,RO]{\small\thepage}
    \fancyhead[RO]{\small\rightmark}
    \fancyhead[LE]{\small Partie~\thepart, \leftmark}
    \fancyfoot[C]{\thepage}
}

\appto\backmatter{\pagestyle{fancy}
\renewcommand{\sectionmark}[1]{\markright{\thesection.\ #1}}
\renewcommand{\chaptermark}[1]{\markboth{\chaptername~\thechapter~--\ #1}{}}
\fancyhead[LE,RO]{\small\thepage}
    \fancyhead[RO]{}
    \fancyhead[LE]{}
    \fancyfoot[C]{\thepage}

}


%%%%%%%%%%%%%% FAIRE DES ENCARTS %%%%%%%%%%%%%%%%%%%%%%%%%%
% Couleurs :
\definecolor{fondtitre}{RGB}{85,85,85}
\definecolor{fonddeboite}{RGB}{232,232,232}
\definecolor{shadecolor}{RGB}{232,232,232}
\ProcessOptions

%%%%%%%%%% Boitemagique  %%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand*{\boitemagique}[2]{
\begin{center}
\begin{tikzpicture}
\node[rectangle,draw=fondtitre!100,fill=fonddeboite!100,inner sep=10pt, inner ysep=20pt] (mabox)
{
\begin{minipage}{12cm}
#2
\end{minipage}
};
\node[fill=fondtitre!100, text=white, rectangle] at (mabox.north) {\sffamily\textbf{#1}};
\end{tikzpicture}
\end{center}
}

%%%%%% boitesimple %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand*{\boitesimple}[1]{%
\begin{center}
\begin{minipage}{12cm}
\begin{shaded}
	#1
\end{shaded}
\end{minipage}
\end{center}
}

\newcommand{\biblio}{
\section*{Bibliographie}
{\setlength{\parindent}{0cm}
\input{biblio}
}
}

% Pour saut de ligne après titre de paragraphe
\makeatletter
\renewcommand{\paragraph}{\@startsection{paragraph}{4}{0ex}%
    {-3.25ex plus -1ex minus -0.2ex}%
    {1.5ex plus 0.2ex}%
    {\normalfont\normalsize\bfseries}}
\makeatother
